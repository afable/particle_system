# created by @afable

c++ = g++
cflags = -Wall -g
libs = -L/lusr/X11/lib -lglut -lGLU  -lGL					# for linux
frameworks = -framework OpenGL -framework GLUT				# for mac
objs = main.o a_vector.o a_matrix.o
prog = particle_system

$(prog): $(objs)
	$(c++) $(cflags) $(objs) $(frameworks) -o $(prog)

%.o: %.cpp
	$(c++) $(cflags) -c $<									# automatic variables

clean:
	rm -f $(objs)
	rm -f $(prog)





