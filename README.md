
Erik Afable
Project: Simple Particle System

---------------------------------------
Compile/Run instructions:
---------------------------------------
	Run make from /src. This project was built only for the MAC/OSX platform.

	This program can either be run after making the build in /src or can be called directly from /bin:
		/bin/particle_system	--> will run the program

	Features while running the program:
	  right mouse button --> toggle polygon surface modes: LINE BACK/FRONT, FILL BACK/FRONT, and CULL BACK.
	  left mouse button --> rotate camera left, right, up, and down.
	  'q' --> quit the program.
	  'w' --> move forward or stop if moving backward.
	  's' --> move backward or stop if moving forward.
	  'a' --> strafe left or stop if strafing right.
	  'd' --> strafe right or stop if strafing left.
	  'r' --> reset position to front of spewing teapot.
	  'p' --> set particle population level (1000, 100, or 10).


---------------------------------------
Solution Methods:
---------------------------------------
	Particle System:

	To create a simple particle system and struct was created for each particle that contained the particle position, speed, and forces applied to the particle. A particle system was used to gather all of the forces and accelerations, apply them to the particles velocity, move the particle, identify collisions with the ground, and then finally draw active particles.

	All particles are intialized in Init() and whether or not a particle appears on screen will depend on if it is active.

	The particle system created was water spewing from a teapot. The amount of particles can be controlled by pressing the 'p' key.


---------------------------------------
Source Acknowledgement:
---------------------------------------
	http://www.naturewizard.com/tutorial08.html							--> particle tutorial and structure
	http://www.pixar.com/companyinfo/research/pbm2001/pdf/notesc.pdf			--> particle structure and phase space information













