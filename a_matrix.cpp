
//
//  a_matrix.cpp
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_matrix.h"

/*
 * CONSTRUCTORS & DESTRUCTORS
 */
AMatrix::AMatrix()
{
	f[0]=1.0f; f[1]=0.0f; f[2]=0.0f; f[3]=0.0f;
	f[4]=0.0f; f[5]=1.0f; f[6]=0.0f; f[7]=0.0f;
	f[8]=0.0f; f[9]=0.0f; f[10]=1.0f; f[11]=0.0f;
	f[12]=0.0f; f[13]=0.0f; f[14]=0.0f; f[15]=1.0f;
}

AMatrix::AMatrix(float m0, float m1, float m2, float m3,
				 float m4, float m5, float m6, float m7,
				 float m8, float m9, float m10, float m11,
				 float m12, float m13, float m14, float m15)
{
	f[0]=m0; f[1]=m1; f[2]=m2; f[3]=m3;
	f[4]=m4; f[5]=m5; f[6]=m6; f[7]=m7;
	f[8]=m8; f[9]=m9; f[10]=m10; f[11]=m11;
	f[12]=m12; f[13]=m13; f[14]=m14; f[15]=m15;
}

AMatrix::AMatrix(float m[16])
{
	f[0]=m[0]; f[1]=m[1]; f[2]=m[2]; f[3]=m[3];
	f[4]=m[4]; f[5]=m[5]; f[6]=m[6]; f[7]=m[7];
	f[8]=m[8]; f[9]=m[9]; f[10]=m[10]; f[11]=m[11];
	f[12]=m[12]; f[13]=m[13]; f[14]=m[14]; f[15]=m[15];
}

AMatrix::AMatrix(const AMatrix & copied)
{
	for (int i = 0; i < M_DIMENSION; ++i)
		f[i] = copied.f[i];
}

AMatrix::AMatrix(const AVector & vRight, const AVector & vUp, const AVector & vForward,
				 const AVector & vOrigin)
{
	f[0]=vRight.f[0]; f[1]=vRight.f[1]; f[2]=vRight.f[2]; f[3]=vRight.f[3];
	f[4]=vUp.f[0]; f[5]=vUp.f[1]; f[6]=vUp.f[2]; f[7]=vUp.f[3];
	f[8]=vForward.f[0]; f[9]=vForward.f[1]; f[10]=vForward.f[2]; f[11]=vForward.f[3];
	f[12]=vOrigin.f[0]; f[13]=vOrigin.f[1]; f[14]=vOrigin.f[2]; f[15]=vOrigin.f[3];
}

AMatrix::~AMatrix()
{
	//	std::cout << "*\t destroying:\n" << *this << "\n... done!" << std::endl;
}

/*
 * MEMBER FUNCTIONS
 */
void AMatrix::Identity()
{
	f[0]=1.0f; f[1]=0.0f; f[2]=0.0f; f[3]=0.0f;
	f[4]=0.0f; f[5]=1.0f; f[6]=0.0f; f[7]=0.0f;
	f[8]=0.0f; f[9]=0.0f; f[10]=1.0f; f[11]=0.0f;
	f[12]=0.0f; f[13]=0.0f; f[14]=0.0f; f[15]=1.0f;
}

void AMatrix::Reset()
{
	Identity();
}

void AMatrix::Transpose()
{
	swap(f[1], f[4]);
	swap(f[2], f[8]);
	swap(f[3], f[12]);
	swap(f[6], f[9]);
	swap(f[7], f[13]);
	swap(f[11], f[14]);
}

void AMatrix::Zero()
{
	for (int i = 0; i < M_DIMENSION; ++i)
		f[i] = 0.0f;
}

AMatrix AMatrix::GetInverse(void)
{
	float fInv[16];
	
	if ( GenerateInverseMatrix(fInv, f) )
		return AMatrix(fInv);
	else
	{
		std::cerr << "Error getting matrix inverse! Returning zero matrix" << std::endl;
		return AMatrix(0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0);
	}
}

float AMatrix::GetDeterminant(const float m[16])
{
	return
	m[12]*m[9]*m[6]*m[3]-
	m[8]*m[13]*m[6]*m[3]-
	m[12]*m[5]*m[10]*m[3]+
	m[4]*m[13]*m[10]*m[3]+
	m[8]*m[5]*m[14]*m[3]-
	m[4]*m[9]*m[14]*m[3]-
	m[12]*m[9]*m[2]*m[7]+
	m[8]*m[13]*m[2]*m[7]+
	m[12]*m[1]*m[10]*m[7]-
	m[0]*m[13]*m[10]*m[7]-
	m[8]*m[1]*m[14]*m[7]+
	m[0]*m[9]*m[14]*m[7]+
	m[12]*m[5]*m[2]*m[11]-
	m[4]*m[13]*m[2]*m[11]-
	m[12]*m[1]*m[6]*m[11]+
	m[0]*m[13]*m[6]*m[11]+
	m[4]*m[1]*m[14]*m[11]-
	m[0]*m[5]*m[14]*m[11]-
	m[8]*m[5]*m[2]*m[15]+
	m[4]*m[9]*m[2]*m[15]+
	m[8]*m[1]*m[6]*m[15]-
	m[0]*m[9]*m[6]*m[15]-
	m[4]*m[1]*m[10]*m[15]+
	m[0]*m[5]*m[10]*m[15];
}

bool AMatrix::GenerateInverseMatrix(float i[16], const float m[16])
{
	float x = GetDeterminant(m);
	if ( x == 0 ) return false;
	
	i[0]= (-m[13]*m[10]*m[7] +m[9]*m[14]*m[7] +m[13]*m[6]*m[11]
		   -m[5]*m[14]*m[11] -m[9]*m[6]*m[15] +m[5]*m[10]*m[15])/x;
	i[4]= ( m[12]*m[10]*m[7] -m[8]*m[14]*m[7] -m[12]*m[6]*m[11]
		   +m[4]*m[14]*m[11] +m[8]*m[6]*m[15] -m[4]*m[10]*m[15])/x;
	i[8]= (-m[12]*m[9]* m[7] +m[8]*m[13]*m[7] +m[12]*m[5]*m[11]
		   -m[4]*m[13]*m[11] -m[8]*m[5]*m[15] +m[4]*m[9]* m[15])/x;
	i[12]=( m[12]*m[9]* m[6] -m[8]*m[13]*m[6] -m[12]*m[5]*m[10]
		   +m[4]*m[13]*m[10] +m[8]*m[5]*m[14] -m[4]*m[9]* m[14])/x;
	i[1]= ( m[13]*m[10]*m[3] -m[9]*m[14]*m[3] -m[13]*m[2]*m[11]
		   +m[1]*m[14]*m[11] +m[9]*m[2]*m[15] -m[1]*m[10]*m[15])/x;
	i[5]= (-m[12]*m[10]*m[3] +m[8]*m[14]*m[3] +m[12]*m[2]*m[11]
		   -m[0]*m[14]*m[11] -m[8]*m[2]*m[15] +m[0]*m[10]*m[15])/x;
	i[9]= ( m[12]*m[9]* m[3] -m[8]*m[13]*m[3] -m[12]*m[1]*m[11]
		   +m[0]*m[13]*m[11] +m[8]*m[1]*m[15] -m[0]*m[9]* m[15])/x;
	i[13]=(-m[12]*m[9]* m[2] +m[8]*m[13]*m[2] +m[12]*m[1]*m[10]
		   -m[0]*m[13]*m[10] -m[8]*m[1]*m[14] +m[0]*m[9]* m[14])/x;
	i[2]= (-m[13]*m[6]* m[3] +m[5]*m[14]*m[3] +m[13]*m[2]*m[7]
		   -m[1]*m[14]*m[7] -m[5]*m[2]*m[15] +m[1]*m[6]* m[15])/x;
	i[6]= ( m[12]*m[6]* m[3] -m[4]*m[14]*m[3] -m[12]*m[2]*m[7]
		   +m[0]*m[14]*m[7] +m[4]*m[2]*m[15] -m[0]*m[6]* m[15])/x;
	i[10]=(-m[12]*m[5]* m[3] +m[4]*m[13]*m[3] +m[12]*m[1]*m[7]
		   -m[0]*m[13]*m[7] -m[4]*m[1]*m[15] +m[0]*m[5]* m[15])/x;
	i[14]=( m[12]*m[5]* m[2] -m[4]*m[13]*m[2] -m[12]*m[1]*m[6]
		   +m[0]*m[13]*m[6] +m[4]*m[1]*m[14] -m[0]*m[5]* m[14])/x;
	i[3]= ( m[9]* m[6]* m[3] -m[5]*m[10]*m[3] -m[9]* m[2]*m[7]
		   +m[1]*m[10]*m[7] +m[5]*m[2]*m[11] -m[1]*m[6]* m[11])/x;
	i[7]= (-m[8]* m[6]* m[3] +m[4]*m[10]*m[3] +m[8]* m[2]*m[7]
		   -m[0]*m[10]*m[7] -m[4]*m[2]*m[11] +m[0]*m[6]* m[11])/x;
	i[11]=( m[8]* m[5]* m[3] -m[4]*m[9]* m[3] -m[8]* m[1]*m[7]
		   +m[0]*m[9]* m[7] +m[4]*m[1]*m[11] -m[0]*m[5]* m[11])/x;
	i[15]=(-m[8]* m[5]* m[2] +m[4]*m[9]* m[2] +m[8]* m[1]*m[6]
		   -m[0]*m[9]* m[6] -m[4]*m[1]*m[10] +m[0]*m[5]* m[10])/x;
	
	return true;
} 

/*
 * ROTATE MATRIX
 */
AMatrix & AMatrix::RotateX(float angle)
{
	*this *= AMatrix(1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, std::cos(angle/RAD_TO_DEGREE), std::sin(angle/RAD_TO_DEGREE), 0.0f,
					 0.0f, -std::sin(angle/RAD_TO_DEGREE), std::cos(angle/RAD_TO_DEGREE), 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
	return *this;
}

AMatrix & AMatrix::RotateY(float angle)
{
	*this *= AMatrix(std::cos(angle/RAD_TO_DEGREE), 0.0f, -std::sin(angle/RAD_TO_DEGREE), 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f,
					 std::sin(angle/RAD_TO_DEGREE), 0.0f, std::cos(angle/RAD_TO_DEGREE), 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
	return *this;
}

AMatrix & AMatrix::RotateZ(float angle)
{
	*this *= AMatrix(std::cos(angle/RAD_TO_DEGREE), std::sin(angle/RAD_TO_DEGREE), 0.0f, 0.0f,
					 -std::sin(angle/RAD_TO_DEGREE), std::cos(angle/RAD_TO_DEGREE), 0.0f, 0.0f,
					 0.0f, 0.0f, 1, 0.0f,
					 0.0f, 0.0f, 0.0f, 1);
	return *this;
}

/*
 * SCALE MATRIX
 */
AMatrix & AMatrix::scale(const float & x, const float & y, const float & z, const float & w)
{
	*this *= AMatrix(x, 0.0f, 0.0f, 0.0f,
					 0.0f, y, 0.0f, 0.0f,
					 0.0f, 0.0f, z, 0.0f,
					 0.0f, 0.0f, 0.0f, w);
	return *this;
}

/*
 * TRANSLATE MATRIX
 */
AVector AMatrix::getTranslation()
{
	return AVector(f[12], f[13], f[14], f[15]);
}

AMatrix & AMatrix::Translate(const float & x, const float & y, const float & z, const float & w)
{
	*this *= AMatrix(1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f,
					 0.0f, 0.0f, 1.0f, 0.0f,
					 x, y, z, w);
	return *this;
}

AMatrix & AMatrix::Translate(const AVector & v)
{
	*this *= AMatrix(1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f,
					 0.0f, 0.0f, 1.0f, 0.0f,
					 v.f[0], v.f[1], v.f[2], v.f[3]);
	return *this;
}

/*
 * OPERATOR OVERLOADS
 */
AMatrix & AMatrix::operator = (const AMatrix & other)
{
	for (int i = 0; i < M_DIMENSION; ++i)
		f[i] = other.f[i];
	return *this;
}

AVector AMatrix::operator * (const AVector & v) const
{
	return AVector(f[0]*v.f[0] + f[4]*v.f[1] + f[8]*v.f[2] + f[12]*v.f[3],
				   f[1]*v.f[0] + f[5]*v.f[1] + f[9]*v.f[2] + f[13]*v.f[3],
				   f[2]*v.f[0] + f[6]*v.f[1] + f[10]*v.f[2] + f[14]*v.f[3],
				   f[3]*v.f[0] + f[7]*v.f[1] + f[11]*v.f[2] + f[15]*v.f[3]);
}

AVector operator * (const AVector & v, const AMatrix & m)
{
	return m * v;
}

AVector & operator *= (AVector & v, const AMatrix & m)
{
	v = m * v;
	return v;
}

bool AMatrix::operator == (const AMatrix & other) const
{
	return (f[0] == other.f[0] && f[1] == other.f[1] && f[2] == other.f[2] && f[3] == other.f[3] &&
			f[4] == other.f[4] && f[5] == other.f[5] && f[6] == other.f[6] && f[7] == other.f[7] &&
			f[8] == other.f[8] && f[9] == other.f[9] && f[10] == other.f[10] && f[11] == other.f[11] &&
			f[12] == other.f[12] && f[13] == other.f[13] && f[14] == other.f[14] && f[15] == other.f[15]);
}

bool AMatrix::operator != (const AMatrix & other) const
{
	return !(*this == other);
}

AMatrix AMatrix::operator *= (const float & k)
{
	for (int i = 0; i < M_DIMENSION; ++i)
		f[i] *= k;
	return *this;
}

AMatrix & AMatrix::operator *= (const AMatrix & m)
{
	*this = *this * m;
	return *this;
}

// return f at col [0,3] and row [0, 3]
float AMatrix::operator () (const unsigned char & col, const unsigned char & row) const
{
	return f[col*COL_SIZE + row];
}

/*
 * FRIEND OPERATOR OVERLOADS
 */
AMatrix operator * (const AMatrix & M, const float & K)
{
	return AMatrix(M.f[0]*K, M.f[1]*K, M.f[2]*K, M.f[3]*K,
				   M.f[4]*K, M.f[5]*K, M.f[6]*K, M.f[7]*K,
				   M.f[8]*K, M.f[9]*K, M.f[10]*K, M.f[11]*K,
				   M.f[12]*K, M.f[13]*K, M.f[14]*K, M.f[15]*K);
}

AMatrix operator * (const float & K, const AMatrix & M)
{
	return M*K;
}

AMatrix operator * (const AMatrix & a, const AMatrix & b)
{
	return AMatrix(a.f[0]*b.f[0] + a.f[4]*b.f[1] + a.f[8]*b.f[2] + a.f[12]*b.f[3],
				   a.f[1]*b.f[0] + a.f[5]*b.f[1] + a.f[9]*b.f[2] + a.f[13]*b.f[3],
				   a.f[2]*b.f[0] + a.f[6]*b.f[1] + a.f[10]*b.f[2] + a.f[14]*b.f[3],
				   a.f[3]*b.f[0] + a.f[7]*b.f[1] + a.f[11]*b.f[2] + a.f[15]*b.f[3],
				   
				   a.f[0]*b.f[4] + a.f[4]*b.f[5] + a.f[8]*b.f[6] + a.f[12]*b.f[7],
				   a.f[1]*b.f[4] + a.f[5]*b.f[5] + a.f[9]*b.f[6] + a.f[13]*b.f[7],
				   a.f[2]*b.f[4] + a.f[6]*b.f[5] + a.f[10]*b.f[6] + a.f[14]*b.f[7],
				   a.f[3]*b.f[4] + a.f[7]*b.f[5] + a.f[11]*b.f[6] + a.f[15]*b.f[7],
				   
				   a.f[0]*b.f[8] + a.f[4]*b.f[9] + a.f[8]*b.f[10] + a.f[12]*b.f[11],
				   a.f[1]*b.f[8] + a.f[5]*b.f[9] + a.f[9]*b.f[10] + a.f[13]*b.f[11],
				   a.f[2]*b.f[8] + a.f[6]*b.f[9] + a.f[10]*b.f[10] + a.f[14]*b.f[11],
				   a.f[3]*b.f[8] + a.f[7]*b.f[9] + a.f[11]*b.f[10] + a.f[15]*b.f[11],
				   
				   a.f[0]*b.f[12] + a.f[4]*b.f[13] + a.f[8]*b.f[14] + a.f[12]*b.f[15],
				   a.f[1]*b.f[12] + a.f[5]*b.f[13] + a.f[9]*b.f[14] + a.f[13]*b.f[15],
				   a.f[2]*b.f[12] + a.f[6]*b.f[13] + a.f[10]*b.f[14] + a.f[14]*b.f[15],
				   a.f[3]*b.f[12] + a.f[7]*b.f[13] + a.f[11]*b.f[14] + a.f[15]*b.f[15]);
}

std::ostream & operator << (std::ostream & os, const AMatrix & m)
{
	os << "{" << m.f[0] << ", " << m.f[4] << ", " << m.f[8] << ", " << m.f[12] << ",\n"
	<< " " << m.f[1] << ", " << m.f[5] << ", " << m.f[9] << ", " << m.f[13] << ",\n"
	<< " " << m.f[2] << ", " << m.f[6] << ", " << m.f[10] << ", " << m.f[14] << ",\n"
	<< " " << m.f[3] << ", " << m.f[7] << ", " << m.f[11] << ", " << m.f[15] << "}";
	
	return os;
}

/* PUBLIC FUNCTIONS */
AMatrix GetRotationFromArbitraryOrthoAxis(float fAngle, AVector vRot)
{
	float u = vRot.f[0]; float v = vRot.f[1]; float w = vRot.f[2];
	fAngle /= RAD_TO_DEGREE;
	
	// math taken from http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/ section 5.2
	return AMatrix(
				   u*u+(1-u*u)*std::cos(fAngle),
				   u*v*(1-std::cos(fAngle))+w*std::sin(fAngle),
				   u*w*(1-std::cos(fAngle))-v*std::sin(fAngle),
				   0,
				   
				   u*v*(1-std::cos(fAngle))-w*std::sin(fAngle),
				   v*v+(1-v*v)*std::cos(fAngle),
				   v*w*(1-std::cos(fAngle))+u*std::sin(fAngle),
				   0,
				   
				   v*w*(1-std::cos(fAngle))+v*std::sin(fAngle),
				   v*w*(1-std::cos(fAngle))-u*std::sin(fAngle),
				   w*w+(1-w*w)*std::cos(fAngle),
				   0,
				   
				   0, 0, 0, 1 );
}

AMatrix GetRotationFromArbitraryOrthoAxis(float fAngle, float u, float v, float w)
{
	fAngle /= RAD_TO_DEGREE;
	
	// math taken from http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/ section 5.2
	return AMatrix(
				   u*u+(1-u*u)*std::cos(fAngle),
				   u*v*(1-std::cos(fAngle))+w*std::sin(fAngle),
				   u*w*(1-std::cos(fAngle))-v*std::sin(fAngle),
				   0,
				   
				   u*v*(1-std::cos(fAngle))-w*std::sin(fAngle),
				   v*v+(1-v*v)*std::cos(fAngle),
				   v*w*(1-std::cos(fAngle))+u*std::sin(fAngle),
				   0,
				   
				   v*w*(1-std::cos(fAngle))+v*std::sin(fAngle),
				   v*w*(1-std::cos(fAngle))-u*std::sin(fAngle),
				   w*w+(1-w*w)*std::cos(fAngle),
				   0,
				   
				   0, 0, 0, 1 );
}

// create a projection to "squish" object into plane. supposedly an easy way of drawing
// shadows onto the ground. must have a plane equation from GetPlaneEquation()
AMatrix MakePlanarShadowMatrix(const AVector & vPlane, const AVector & vLightPos)
{
	// make code easier to read.
	float a = vPlane.f[0];
	float b = vPlane.f[1];
	float c = vPlane.f[2];
	float d = vPlane.f[3];
	
	float dx = -vLightPos.f[0];
	float dy = -vLightPos.f[1];
	float dz = -vLightPos.f[2];
	
	// now build and return the projection matrix
	return AMatrix( b * dy + c * dz,
				   -a * dy,
				   -a * dz,
				   0.0f,
				   
				   -b * dx,
				   a * dx + c * dz,
				   -b * dz,
				   0.0f,
				   
				   -c * dx,
				   -c * dy,
				   a * dx + b * dy,
				   0.0f,
				   
				   -d * dx,
				   -d * dy,
				   -d * dz,
				   a * dx + b * dy + c * dz
				   );
}




