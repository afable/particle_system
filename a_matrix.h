//
//  a_matrix.h
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_MATRIX_H_
#define A_MATRIX_H_

#include "a_vector.h"
#include <iostream> // for std::ostream
#include <cmath>

const float RAD_TO_DEGREE = 57.29577958;

enum {M_DIMENSION = 16, COL_SIZE = 4, ROW_SIZE = 4};

class AMatrix
{
private:
	void swap(float & f1, float & f2)
	{
		float temp;
		temp = f1;
		f1 = f2;
		f2 = temp;
	}
public:
	float f[16];  // opengl style matrix, remember: column ordered!
	
	/* CONSTRUCTORS & DESTRUCTORS */
	AMatrix();
	AMatrix(float m0, float m1=0, float m2=0, float m3=0,
			float m4=0, float m5=1, float m6=0, float m7=0,
			float m8=0, float m9=0, float m10=1, float m11=0,
			float m12=0, float m13=0, float m14=0, float m15=1);
	AMatrix(float m[16]);
	AMatrix(const AMatrix & copied);
	AMatrix(const AVector & vRight, const AVector & vUp, const AVector & vForward,
			const AVector & vOrigin = AVector(0.0f, 0.0f, 0.0f, 1.0f));
	~AMatrix();
	
	/* MEMBER FUNCTIONS */
	void print() const;
	void Identity();
	void Reset();
	void Transpose();
	void Zero();
	AMatrix GetInverse(void);
	float GetDeterminant(const float m[16]);
	bool GenerateInverseMatrix(float i[16], const float m[16]);
	
	/* ROTATE MATRIX */
	AMatrix & RotateX(float angle); // positive angle = ccw
	AMatrix & RotateY(float angle);
	AMatrix & RotateZ(float angle);
		
	/* SCALE MATRIX */
	AMatrix & scale(const float & x, const float & y, const float & z, const float & w=1);
	
	/* TRANSLATE MATRIX */
	AVector getTranslation();
	AMatrix & Translate(const float & x, const float & y, const float & z, const float & w=1);
	AMatrix & Translate(const AVector & v);
	
	/* OPERATOR OVERLOADS */
	AMatrix & operator = (const AMatrix & other);
	AVector operator * (const AVector & v) const;
	friend AVector operator * (const AVector & v, const AMatrix & m);
	friend AVector & operator *= (AVector & v, const AMatrix & m);
	bool operator == (const AMatrix & other) const;
	bool operator != (const AMatrix & other) const;
	AMatrix operator *= (const float & k);
	AMatrix & operator *= (const AMatrix & m);
	
	float operator () (const unsigned char & col, const unsigned char & row) const;	
	
	/* FRIEND OPERATOR OVERLOADS */
	friend AMatrix operator * (const AMatrix & M, const float & K);
	friend AMatrix operator * (const float & K, const AMatrix & M);
	friend AMatrix operator * (const AMatrix & lhm, const AMatrix & rhm);
	friend std::ostream & operator << (std::ostream & os, const AMatrix & m);
	
}; // end of class AMatrix


/* PUBLIC FUNCTIONS */
// seems to work... not fully convinced though.
AMatrix GetRotationFromArbitraryOrthoAxis(float fAngle, AVector vRot);
AMatrix GetRotationFromArbitraryOrthoAxis(float fAngle, float u, float v, float w);
AMatrix MakePlanarShadowMatrix(const AVector & vPlane, const AVector & vLightPos);





#endif // A_MATRIX_H_


