//
//  a_vector.cpp
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_vector.h"
#include <cmath>

enum {V_DIMENSION = 4};

//	constructors and destructor
AVector::AVector()
{
	f[0] = f[1] = f[2] = f[3] = 0.0f;
}

AVector::AVector(const float x, const float y, const float z, const float w)
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

AVector::AVector(const AVector & copied)
{
	for (int i = 0; i < V_DIMENSION; ++i)
		f[i] = copied.f[i];
}

AVector::~AVector()
{
	//	do-nothing silent destructor
}

//	setters
void AVector::Set(const float x, const float y, const float z, const float w)
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

void AVector::Normalize() // makes vector unit length
{
	float magnitude = GetMag();
	
	for (int i = 0; i < V_DIMENSION; ++i)
		f[i] /= magnitude;
}

void AVector::Zero() // assigns zeros to all vector values
{
	f[0] = f[1] = f[2] = f[3] = 0.0f;
}

//	getters
float AVector::GetMag(void) // returns length of vector
{
	if ( f[3] == 0 )
		return std::sqrt(f[0]*f[0] + f[1]*f[1] + f[2]*f[2] + f[3]*f[3]);
	else
	{
		std::cerr << "Cannot get magnitude of a point! AVector::GetMag() failed." << std::endl;
		return 1.0f;
	}
}

AVector AVector::GetUnitVector(void)
{
	AVector u = *this;
	u.Normalize();
	return u;
}

// class methods
bool AVector::IsPoint(void) const
{
	return f[3] == 1.0f;
}

//	operator overloads
AVector & AVector::operator = (const AVector & other)
{
	f[0] = other.f[0];
	f[1] = other.f[1];
	f[2] = other.f[2];
	f[3] = other.f[3];
	
	return *this;
}

// cannot use for cout << (happy + sad); returned copy gets destroyed before going to cout? only works for assignment
AVector AVector::operator + (const AVector & other)
{
	return AVector(f[0]+other.f[0], f[1]+other.f[1], f[2]+other.f[2], f[3]+other.f[3]);
}

AVector AVector::operator += (const AVector & other)
{
	f[0] += other.f[0];
	f[1] += other.f[1];
	f[2] += other.f[2];
	f[3] += other.f[3];
	
	return *this;
}

AVector AVector::operator -= (const AVector & other)
{
	f[0] -= other.f[0];
	f[1] -= other.f[1];
	f[2] -= other.f[2];
	f[3] -= other.f[3];
	
	return *this;
}

AVector AVector::operator *= (const float & scalar)
{
	f[0] *= scalar;
	f[1] *= scalar;
	f[2] *= scalar;
	
	return *this;
}

bool AVector::operator == (const AVector & other)
{
	return (f[0] == other.f[0] && f[1] == other.f[1] && f[2] == other.f[2] && f[3] == other.f[3]);
}

bool AVector::operator != (const AVector & other)
{
	return (f[0] != other.f[0] || f[1] != other.f[1] || f[2] != other.f[2] || f[3] != other.f[3]);
}

//	friend operator overloads
AVector operator * (const float & s, const AVector & v)
{
	return AVector(s*v.f[0], s*v.f[1], s*v.f[2]);
}

AVector operator * (const AVector & v, const float & s)
{
	return s*v;
}

AVector operator - (const AVector & vLeft, const AVector & vRight)
{
	return AVector(vLeft.f[0]-vRight.f[0],
				   vLeft.f[1]-vRight.f[1],
				   vLeft.f[2]-vRight.f[2],
				   vLeft.f[3]-vRight.f[3]);
}

std::ostream & operator << (std::ostream & os, AVector & v)
{
	os << "{" << v.f[0] << "," << v.f[1] << "," << v.f[2] << "," << v.f[3] << "}";
	
	return os;
}

/* 
 * PUBLIC FUNCTIONS
 */
AVector CrossProduct(const AVector & LHV, const AVector & RHV)
{
	if (LHV.f[3] == 0 && RHV.f[3] == 0)
		return AVector(LHV.f[1]*RHV.f[2] - LHV.f[2]*RHV.f[1],
					   LHV.f[2]*RHV.f[0] - LHV.f[0]*RHV.f[2],
					   LHV.f[0]*RHV.f[1] - LHV.f[1]*RHV.f[0]);
	else
	{
		std::cout << "Can only cross_product vectors! Aborting.\n";
		return AVector(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

float DotProduct(const AVector & LHV, const AVector & RHV)
{
	if ( LHV.f[3] == 0 && RHV.f[3] == 0 )
		return (LHV.f[0]*RHV.f[0] + LHV.f[1]*RHV.f[1] + LHV.f[2]*RHV.f[2] + LHV.f[3]*RHV.f[3]);
	else
	{
		std::cout << "Can only dot_product vectors! DotProduct failed." << std::endl;
		return 0.0f;
	}
}

// return a normal unit vector from the cross product of three points
AVector FindNormal(const AVector & vP1, const AVector & vP2, const AVector & vP3)
{
	if ( !vP1.IsPoint() || !vP2.IsPoint() || !vP3.IsPoint() )
	{
		std::cerr << "All vectors must be points! FindNormal() failed." << std::endl;
		return AVector();
	}
	AVector u = CrossProduct(vP2 - vP1, vP3 - vP1);
	u.Normalize();
	return u;
}

// return the equation of a plane (Ax + By + Cz + D = 0) which has to do with determinants
// points should be given in cw order with normal pointing out of cw face
AVector GetPlaneEquation(const AVector & vP1, const AVector & vP2, const AVector & vP3)
{
	// get two vectors... do the cross product
	AVector v1 = vP3 - vP1;
	AVector v2 = vP2 - vP1;
	
	// get unit normal to plane
	AVector u = CrossProduct(v1, v2);
	
	// calculate the d value of the plane and add to u and you have the equation of a plane!
	u.f[3] = -(u.f[0]*vP3.f[0] + u.f[1]*vP3.f[1] + u.f[2]*vP3.f[2]);
	
	return u;
}
















