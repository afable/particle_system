//
//  a_vector.h
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_VECTOR_H_
#define A_VECTOR_H_

#include <iostream> // for std::ostream

// semantic meaning for vector dimensions
enum { x=0, y, z, w };

class AVector
{
public:
	float f[4];
	
	//	constructors and destructor
	AVector();
	AVector(const float x, const float y, const float z, const float w=0);
	AVector(const AVector & copied);
	~AVector();
	
	//	setters
	void Set(const float x, const float y, const float z, const float w=0);
	void Normalize(); // makes vector unit length
	void Zero(); // assigns zeros to all vector values
	//	setters (inline)
	inline void SetX(const float & val) { f[0] = val; }
	inline void SetY(const float & val) { f[1] = val; }
	inline void SetZ(const float & val) { f[2] = val; }
	inline void SetW(const float & val) { f[3] = val; }
	
	//	getters
	float GetMag(); // returns length of vector
	AVector GetUnitVector(void); // returns a unit vector of this vector. no side effects
	//	getters (inline)
	inline float GetX() { return f[0]; }
	inline float GetY() { return f[1]; }
	inline float GetZ() { return f[2]; }
	inline float GetW() { return f[3]; }
	
	// class methods
	bool IsPoint(void) const;
	
	//	operator overloads
	AVector & operator = (const AVector & other);
	AVector operator + (const AVector & other);
	
	AVector operator += (const AVector & other);
	AVector operator -= (const AVector & other);
	AVector operator *= (const float & scalar);
	
	bool operator == (const AVector & other);
	bool operator != (const AVector & other);
	
	//	friend operator overloads
	friend AVector operator * (const float & s, const AVector & v);
	friend AVector operator * (const AVector & v, const float & s);
	friend AVector operator - (const AVector & vLeft, const AVector & vRight);
	friend std::ostream & operator << (std::ostream & os, AVector & v);
}; // end of class AVector

/*
 * PUBLIC FUNCTIONS: other classes 
 */
AVector CrossProduct(const AVector & LHV, const AVector & RHV); 
float DotProduct(const AVector & LHV, const AVector & RHV);
AVector FindNormal(const AVector & vP1, const AVector & vP2, const AVector & vP3);
AVector GetPlaneEquation(const AVector & vP1, const AVector & vP2, const AVector & vP3);




#endif  // A_VECTOR_H_


