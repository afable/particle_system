//
//  main.cpp
//  AFABLEE_PROJ5_MAC
//
//  Created by afable on 12-04-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/GLUT.h>

#include <stdlib.h> // for srand(uint) and rand(void)
#include <time.h>	// for time(NULL) to seed srand

#include "a_vector.h"
#include "a_matrix.h"


#define NEGATIVE -

// particle stuffs
typedef struct
{
	int id;
	float lifetime;                       // total lifetime of the particle
	float decay;                          // decay speed of the particle
	float r, g, b, a;                          // color values of the particle
	float xpos, ypos, zpos;                 // position of the particle
	float xspeed, yspeed, zspeed;           // speed of the particle
	bool active;                       // is particle active or not?
} Particle;

// file scope unnamed namespace
namespace 
{
	// window properties
	const int WINDOW_X_POS = 0;
	const int WINDOW_Y_POS = 0;
	GLsizei nWindowWidth = 1280;
	GLsizei nWindowHeight = 720;
	
	// frames per second
	const unsigned int GLUT_TIMER_DELAY = 16; // ~60 fps
	
	// feature processing menu
	const int MENU_BACK_LINE = 0;
	const int MENU_FRONT_LINE = 1;
	const int MENU_BACK_FILL = 2;
	const int MENU_FRONT_FILL = 3;
	const int MENU_CULL = 4;
	GLboolean bCull = false;
	
	// mouse features
	GLboolean bMouseDown = false;
	GLint nPrevX = 0;
	GLint nPrevY = 0;
	GLint nSensitivity = 2;
	
	// file scope particle pointer and properties
	Particle *p;
	int populator = 1;
	int NUM_PARTICLES = 1000;
	float X_SPEED = 0.00005f;
	float Y_SPEED = 0.0001f;
	float Z_SPEED = 0.00001f;
	const float X_START_POS = 1.6f;
	const float Y_START_POS = 0.2f;
	const float Z_START_POS = 0.0f;
	
	// file scope particle forces and accelerations
	float xfriction = 1.01f;
	float yfriction = 1.0f;
	float zfriction = 1.0f;
	float xaccel = 0.0f;
	float yaccel = -0.001f;
	float zaccel = 0.0f;
	
	// function prototypes
	void Init(void);
	void Timer(int value);
	void Display(void);
	
	void DrawGround(GLfloat fExtent, GLfloat fStep, GLfloat fHeight);

	void Keyboard(GLubyte key, int x, int y);
	void Special(int key, int x, int y);
	void Mouse(int button, int state, int x, int y);
	void Motion(int x, int y);

	void ProcessMenu(int mode);
	void Reshape(GLsizei w, GLsizei h);
	void Intro(void);
	
	// object drawing rotations and translations with respect to camera (appear as if camera moves)
	GLfloat fRotX = 0.0f;
	GLfloat fRotY = 0.0f;
	const float ROTATION_SPEED = 5.0f;
	GLfloat fForwardSpeed = 0.0f;
	GLfloat fUpSpeed = 0.0f;
	GLfloat fRightSpeed = 0.0f;
	const float TRANSLATION_SPEED = 0.1f;
	GLboolean bGoingUp = false;
	
	// eye features
	AVector pPosition = AVector(0.0f, 0.0f, 0.0f, 1.0f);
	AVector vForward = AVector(0.0f, 0.0f, -1.0f, 0.0f);
	AVector	vUp = AVector(0.0f, 1.0f, 0.0f, 0.0f);
	// vRight is calculated from vForward cross_product vUp
	AMatrix mEye;
}


int main(int argc, char **argv)
{
	// init glut
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(::WINDOW_X_POS, ::WINDOW_Y_POS);
	glutInitWindowSize(::nWindowWidth, ::nWindowHeight);
	
	int window;
	window = glutCreateWindow("particle system");
	
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutTimerFunc(::GLUT_TIMER_DELAY, Timer, 0);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(Special);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	
	// init opengl states and other systems
	Init();
	
	// create right-clickable feature processing menu for polygons
	int nMainMenu;
	nMainMenu = glutCreateMenu(ProcessMenu);
	glutAddMenuEntry("line back", ::MENU_BACK_LINE);
	glutAddMenuEntry("line front", ::MENU_FRONT_LINE);
	glutAddMenuEntry("fill back", ::MENU_BACK_FILL);
	glutAddMenuEntry("fill front", ::MENU_FRONT_FILL);
	glutAddMenuEntry("cull back", ::MENU_CULL);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	
	// start glut's magical main loop
	glutMainLoop();
	
	return 0;
}

namespace 
{
	void Init(void)
	{
		glClearColor(0.8f, 0.7f, 0.6f, 1.0f);
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		
		// enable depth testing
		glEnable(GL_DEPTH_TEST);
		
		// enable smooth shading progression between colored vertices
		glShadeModel(GL_SMOOTH);
		
		// draw front face polygons ccw (default)
		glEnable(GL_CCW);
		
		// set cout to display 2 decimals for vector and matrix debugging
		std::cout.std::ios_base::setf(std::ios::fixed);
		std::cout.std::ios_base::precision(2);
		
		// set opengl point size for particle system
		glPointSize(30.0f);
		
		// enable blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		
		// antialiasing method (may work better for points than multisampling)
		glEnable(GL_POINT_SMOOTH);
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glEnable(GL_POLYGON_SMOOTH);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		
		// intialize particles
		p = (Particle *)malloc(sizeof(Particle) * ::NUM_PARTICLES);
		for ( int i = 0; i < ::NUM_PARTICLES; ++i)
		{
			p[i].id = i;
			
			srand(i);
			p[i].lifetime = 1000.0f;
			p[i].decay = 1.0f;
			p[i].r = 0.2f;
			p[i].g = 0.2f;
			p[i].b = 0.5f;
			p[i].a = 1.0f;
			p[i].xpos = ::X_START_POS;
			p[i].ypos = ::Y_START_POS;
			p[i].zpos = ::Z_START_POS;
			p[i].xspeed = ::X_SPEED * (float)(rand() % ::NUM_PARTICLES);
			p[i].yspeed = ::Y_SPEED * (float)(rand() % ::NUM_PARTICLES);
			p[i].zspeed = ::Z_SPEED * (float)(rand() % ::NUM_PARTICLES);
			p[i].active = true;
		}
		
		// print introduction
		Intro();
	}
}

namespace 
{
	void Timer(int value)
	{
		glutPostRedisplay();
		glutTimerFunc(::GLUT_TIMER_DELAY, Timer, 0);
	}
}

namespace 
{
	void Display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glPushMatrix(); // objects-in-front-of-camera transformations
		{
			/**********************************************
			 * PERFORM CAMERA TRANFORMATIONS: Perform initial coordinate transformations to draw
			 * objects-in-front-of-camera in a translated and rotated perspective from original
			 * camera position (eye of camera is always in the same position, objects are just
			 * distorted around this eye to simulate camera movement). Do this for world crawling.
			 * Draw objects that will always appear in front of camera first such as sky or a
			 * character in 3rd ppov.
			 *
			 * Note: OpenGL coords follow right hand rule. +x right, +y up, +z out of screen.
			 **********************************************/
			
			// translate forward in -z and start drawing stuff for the eye to see in front of it
			glTranslatef(0.0f, 0.0f, 0.0f);	// no translation since we want first person pov
			// rotate object before drawing to appear as if camera is rotating around the object
			glRotatef(::fRotX, 1.0f, 0.0f, 0.0f);
			glRotatef(::fRotY, 0.0f, 1.0f, 0.0f);
			// want to start out +5.0f away from the spewing teapot, so draw stuff -5.0f in the -z
			glTranslatef(0.0f, 0.0f, -5.0f);
			
			
			
			/**********************************************
			 * PERFORM EYE TRANSLATIONS: Perform forward translation (towards eye direction).
			 **********************************************/
			
			// setup eye translation vectors to right directions
			::mEye.Reset();
			::mEye.RotateY(::fRotY);
			
			// set forward vector
			::vForward.Set(0.0f, 0.0f, -1.0f);	// initial forward direction is facing into screen
			::vForward *= ::mEye;	// rotate forward vector based on y rotation
			::vForward.Normalize();		// ensure forward vector is still normal (precision errors)
			
			// set up vector
			::vUp.Set(0.0f, 1.0f, 0.0f);
			
			// create and set right vector based on forward and up vector
			AVector vRight = CrossProduct(::vForward, ::vUp);
			
			// increment eye position opposite direction of forward to draw objects (makes eye appear to move forward)
			::vForward *= ::fForwardSpeed;
			::pPosition += ::vForward;		// increment eye position from forward vector (will negate when actually translating)
			
			// increment eye position based on direction of right to draw objects (makes eye appear to strafe right)
			vRight *= fRightSpeed;
			::pPosition += vRight;		// increment eye position from right vector
			
			// increment eye position based on direction of up to draw objects (makes eye appear to translate up)
			::vUp *= ::fUpSpeed;
			::pPosition += ::vUp;
			
			// finally, translate the actual eye by pPosition! remember, forward position is opposite to make eye appear to be moving in that direction (forward vector is in -z, and --z = +z, so drawer moves in +z before drawing stuff to make it appear that eye has moved passed the stuff that will be drawn!)
			glTranslatef(::pPosition.f[::x], ::pPosition.f[::y], NEGATIVE::pPosition.f[::z]);

			
			
			/**********************************************
			 * DRAW NON-MOVING OBJECTS: Go further into the world and draw non-moving objects.
			 * These objects will be drawn with respect to the initial rotation and transformation
			 * coordinates that were used to setup the objects-in-front-of-camera. Non-moving
			 * objects include ground and environment (trees, etc.) and will not appear "attached"
			 * to the camera's perceived movement.
			 **********************************************/
			
			glPushMatrix(); // draw ground
			{
				glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
				DrawGround(100.0f, 1.0f, -0.5f);
			}
			glPopMatrix(); // end draw ground
			
			glPushMatrix(); // draw triangle in background
			{
				// translate forward in -z from objects-in-front-of-camera coordinate system to draw stuff that appears to rotate within the world
				glTranslatef(0.0f, 0.0f, 0.0f);
				glRotatef(10.0f, 0.0f, 0.0f, -1.0f);
				glutWireTeapot(1.0f);
			}
			glPopMatrix(); // end draw triangle in background
			
			
			
			/**********************************************
			 * DRAW MOVING OBJECTS: Reset coordinate system to objects-in-front-of-camera and go
			 * into world and draw and transform all moving objects (actors).
			 **********************************************/
			
			glPushMatrix(); // actor transformations
			{
				Particle *it = p;
				
				for ( ; it < p+::NUM_PARTICLES; it++ )
				{
					// only draw particle if it is active
					if ( it->active )
					{
						// draw particle
						glBegin(GL_POINTS); {
							glColor4f(it->r, it->g, it->b, it->a);
							glVertex3f(it->xpos, it->ypos, it->zpos);
						} glEnd();
						
						// decrement particle life time
						it->lifetime -= it->decay;
						
						// deactivate particle if it is past its lifetime
						if ( it->lifetime < 0 )
							it->active = false;
						else
						{
							// increment speeds from accelerations
							it->xspeed += xaccel;
							it->yspeed += yaccel;
							it->zspeed += zaccel;
							
							// decrement speeds from friction
							it->xspeed /= xfriction;
							it->yspeed /= yfriction;
							it->zspeed /= zfriction;
							
							// increment positions from speeds
							it->xpos += it->xspeed;
							it->ypos += it->yspeed;
							it->zpos += it->zspeed;
							
							// increment particle colors
							it->r += 0.003f;
							it->b += 0.01f;
							it->g += 0.003f;
							it->a -= 0.01f; // particle slowly disappears
							
							// deactivate particle if it has collided with floor
							if ( it->ypos < -0.5f )
								it->active = false;
						}
					}
					else
					{
						// re-activate particle!
						srand( (int)(it->ypos + it->xpos + it->zpos + it->lifetime) + (int)(time(NULL) % ::NUM_PARTICLES) );
						it->lifetime = 1000.0f;
						it->decay = 1.0f;
						it->r = 0.2f;
						it->g = 0.2f;
						it->b = 0.5f;
						it->a = 1.0f;
						it->xpos = ::X_START_POS;
						it->ypos = ::Y_START_POS;
						it->zpos = ::Z_START_POS;
						it->xspeed = ::X_SPEED * (float)(rand() % ::NUM_PARTICLES);
						it->yspeed = ::Y_SPEED * (float)(rand() % ::NUM_PARTICLES);
						it->zspeed = ::Z_SPEED * (float)(rand() % ::NUM_PARTICLES);
						it->active = true;
					}
				}
			}
			glPopMatrix(); // end actor transformations
		}
		glPopMatrix(); // end objects-in-front-of-camera transformations
		
		
		glutSwapBuffers();
	}
}

namespace 
{
	void DrawGround(GLfloat fExtent, GLfloat fStep, GLfloat fHeight)
	{	
		GLfloat fLine;
		glBegin(GL_LINES);
		{
			for ( fLine = -fExtent; fLine <= fExtent; fLine += fStep )
			{
				// draw z lines
				glVertex3f(fLine, fHeight, -fExtent);
				glVertex3f(fLine, fHeight, fExtent);
				
				// draw x lines
				glVertex3f(-fExtent, fHeight, fLine);
				glVertex3f(fExtent, fHeight, fLine);
			}
		}
		glEnd();
	}
}

namespace 
{
	void Keyboard(GLubyte key, int x, int y)
	{
		switch ( key )
		{
			case 'q': exit(EXIT_SUCCESS);
				break;
			case 'p':
				::populator *= 10;
				if ( ::populator > 100 )
					::populator = 1;
					
				::NUM_PARTICLES = 1000 / ::populator;
				::X_SPEED = 0.00005f * ::populator;
				::Y_SPEED = 0.0001f * ::populator;
				::Z_SPEED = 0.00001f * ::populator;
				
				std::cout << "population set to " << ::NUM_PARTICLES << std::endl;
				
				break;
				
			case 'w':
				if ( ::fForwardSpeed < 0 )
					::fForwardSpeed = 0;
				else
					::fForwardSpeed = ::TRANSLATION_SPEED;
				break;
			case 's':
				if ( ::fForwardSpeed > 0 )
					::fForwardSpeed = 0;
				else
					::fForwardSpeed = NEGATIVE::TRANSLATION_SPEED;
				break;
			case 'a':
				if ( ::fRightSpeed < 0 )
					::fRightSpeed = 0;
				else
					::fRightSpeed = ::TRANSLATION_SPEED;
				break;
			case 'd':
				if ( ::fRightSpeed > 0 )
					::fRightSpeed = 0;
				else
					::fRightSpeed = NEGATIVE::TRANSLATION_SPEED;
				break;
			case ' ':
				if ( ::fUpSpeed != 0 )
				{
					::fUpSpeed = 0;
					::bGoingUp = !::bGoingUp;
				}
				else
				{
					::bGoingUp? ::fUpSpeed = ::TRANSLATION_SPEED : ::fUpSpeed = NEGATIVE::TRANSLATION_SPEED;
				}	
				break;
			case 'r':
				::pPosition.Set(0.0f, 0.0f, 0.0f);
				::fRotX = 0.0f;
				::fRotY = 0.0f;
				::bGoingUp = false;
				break;
				
			default:
				break;
		}
		glutPostRedisplay();
	}
}

namespace 
{
	void Special(int key, int x, int y)
	{
		switch ( key )
		{
			case GLUT_KEY_UP:
				break;
			case GLUT_KEY_DOWN:
				break;
			case GLUT_KEY_LEFT:
				break;
			case GLUT_KEY_RIGHT:
				break;
				
			default:
				break;
		}
		glutPostRedisplay();
	}
}

namespace 
{
	void Mouse(int button, int state, int x, int y)
	{
		switch ( button )
		{
			case GLUT_LEFT_BUTTON: switch ( state )
			{
				case GLUT_DOWN: ::bMouseDown = true;
					break;
				case GLUT_UP: ::bMouseDown = false;
					break;
					
				default:
					break;
			}
				break;
				
			default:
				break;
		}
		glutPostRedisplay();
	}
}

namespace 
{
	void Motion(int x, int y)
	{
		if ( ::bMouseDown )
		{
			if ( x > ::nPrevX + ::nSensitivity )
			{
				::fRotY += ROTATION_SPEED;
			}
			else if ( x < ::nPrevX - ::nSensitivity )
			{
				::fRotY -= ROTATION_SPEED;
			}
			
			if ( y > ::nPrevY + ::nSensitivity )
			{
				::fRotX += ROTATION_SPEED;		// remember! ++y is down screen!
			}
			else if ( y < ::nPrevY - ::nSensitivity )
			{
				::fRotX -= ROTATION_SPEED;		// remember! --y is up screen!
			}
		}
		// update x, y values for next iteration
		::nPrevX = x;
		::nPrevY = y;
		
		glutPostRedisplay();
	}
}

namespace 
{
	void ProcessMenu(int mode)
	{
		switch ( mode )
		{
			case ::MENU_BACK_LINE: glPolygonMode(GL_BACK, GL_LINE);
				break;
			case ::MENU_FRONT_LINE: glPolygonMode(GL_FRONT, GL_LINE);
				break;
			case ::MENU_BACK_FILL: glPolygonMode(GL_BACK, GL_FILL);
				break;
			case ::MENU_FRONT_FILL: glPolygonMode(GL_FRONT, GL_FILL);
				break;
			case ::MENU_CULL:
				::bCull = !::bCull;
				::bCull? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
				break;
				
			default:
				break;
		}
		glutPostRedisplay();
	}
}

namespace 
{
	void Reshape(GLsizei w, GLsizei h)
	{	
		// handle division by zero
		if ( !h )
			h = 1;
		
		GLdouble fAspect;
		fAspect = (GLdouble)w / (GLdouble)h;
		
		// update screen width and height
		::nWindowWidth = w;
		::nWindowHeight = h;
		
		// set viewport to window dimensions
		glViewport(0, 0, w, h);
		
		// reset coordinate system for what is projected to screen
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		// set perspective view
		GLdouble fovY = 30.0;
		gluPerspective(fovY*2.0, fAspect, 0.1, 1000.0);
		
		// load modelview for transforming objects
		glMatrixMode(GL_MODELVIEW);
	}
}

namespace 
{
	void Intro(void)
	{
		std::cout << "\nWelcome to Spewing Teapot World! Let me give you some information about this world.\n";
		std::cout << "\tRun:\n";
		std::cout << "\t\t/bin/particle_system\t\t\t// to run program\n";
		std::cout << "\tModes:\n";
		std::cout << "\t\tright click main window and toggle polygon surface modes: LINE BACK/FRONT, FILL BACK/FRONT, and CULL BACK\n";
		std::cout << "\tUsage:\n";
		std::cout << "\t\t-q --> quit program\n";
		std::cout << "\t\t-left mouse button & drag --> rotate left, right, up, and down\n";
		std::cout << "\t\t-'w' --> move forward or stop if moving backward\n";
		std::cout << "\t\t-'s' --> move backward or stop if moving forward\n";
		std::cout << "\t\t-'a' --> strafe left or stop if strafing right";
		std::cout << "\t\t-'d' --> straft right or stop if strafing left";
		std::cout << "\t\t-'r' --> reset position to front of spewing teapot\n";
		std::cout << "\t\t-'p' --> set particle population level (1000, 100, or 10)\n\n";
	}
}















